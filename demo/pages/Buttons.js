import React, { Component, PropTypes } from 'react';
import Color from '../../src/color';
import { Button, Container, Row, Col } from '../../src/Rubrick';

export class Buttons extends Component {
  render() {
    return (
      <Container>
        <Row styles={{marginTop: 20, marginBottom: 20}}>
          <Col sm='1/3'>
            <h2>Button Sizing</h2>
            <div style={{margin: '2rem 0'}}><Button size='lg'>Large Button</Button></div>
            <div style={{margin: '2rem 0'}}><Button>Default Button</Button></div>
            <div style={{margin: '2rem 0'}}><Button size='sm'>Small Button</Button></div>
            <div style={{margin: '2rem 0'}}><Button size='xs'>Extra Small Button</Button></div>
            <div style={{margin: '2rem 0'}}><Button block>Block Button</Button></div>
          </Col>
          <Col sm='1/3'>
            <h2>Button Color</h2>
            <div style={{margin: '2rem 0'}}><Button>Default Color</Button></div>
            <div style={{margin: '2rem 0'}}><Button shade='dark' backgroundColor={Color.material.teal500}>Material teal-500</Button></div>
            <div style={{margin: '2rem 0'}}><Button shade='dark' backgroundColor={Color.material.indigo500}>Material indigo-500</Button></div>
            <div style={{margin: '2rem 0'}}><Button shade='dark' backgroundColor={Color.material.red500}>Material red-500</Button></div>
            <div style={{margin: '2rem 0'}}><Button shade='dark' backgroundColor={Color.material.orange500}>Material orange-500</Button></div>
          </Col>
          <Col sm='1/3'>
            <h2>Button Type</h2>
            <div style={{margin: '2rem 0'}}><Button>{`<button>`} Button</Button></div>
            <div style={{margin: '2rem 0'}}><Button anchor href='#'>{`<a>`} Button</Button></div>
            <div style={{margin: '2rem 0'}}><Button disabled>Disabled Button</Button></div>
            <div style={{margin: '2rem 0'}}><Button shade='dark' backgroundColor={Color.material.red500} disabled>Disabled Button</Button></div>
          </Col>
        </Row>
      </Container>
    );
  }
}
