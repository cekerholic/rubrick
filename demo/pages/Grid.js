import React, { Component } from 'react';
import Color from '../../src/color';
import { Container, Row, Col } from '../../src/Rubrick';

export class Grid extends Component {

  render() {
    return (
      <Container>
        <h2>Grid</h2>
        <Row styles={{marginBottom: 24, textAlign: 'center'}}>
          <Col>
            <div style={{padding: '15px 0', backgroundColor: Color.material.lightBlue500, color: Color.material.white}}>Full column</div>
          </Col>
        </Row>
        <Row styles={{marginBottom: 24, textAlign: 'center'}}>
          <Col xs='1/2' md='1/3'>
            <div style={{padding: '15px 0', backgroundColor: Color.material.pink600, color: Color.material.white}}>Desktop 33%, Mobile 50%</div>
          </Col>
          <Col xs='1/2' md='1/3'>
            <div style={{padding: '15px 0', backgroundColor: Color.material.pink400, color: Color.material.white}}>Desktop 33%, Mobile 50%</div>
          </Col>
        </Row>

        <Row styles={{marginBottom: 24, textAlign: 'center'}}>
          <Col sm='28%'>
            <div style={{padding: '15px 0', backgroundColor: Color.material.green400, color: Color.material.white}}>Width 28%</div>
          </Col>
          <Col sm='72%'>
            <div style={{padding: '15px 0', backgroundColor: Color.material.green600, color: Color.material.white}}>Width 72%</div>
          </Col>
        </Row>

        <Row styles={{marginBottom: 24, textAlign: 'center'}}>
          <Col sm='1/4'>
            <div style={{padding: '15px 0', backgroundColor: Color.material.purple400, color: Color.material.white}}>one quarter</div>
          </Col>
          <Col sm='1/2'>
            <div style={{padding: '15px 0', backgroundColor: Color.material.purple600, color: Color.material.white}}>one half</div>
          </Col>
          <Col sm='1/4'>
            <div style={{padding: '15px 0', backgroundColor: Color.material.purple400, color: Color.material.white}}>one quarter</div>
          </Col>
        </Row>

        <Row styles={{marginBottom: 24, textAlign: 'center'}}>
          <Col pushSm='1/2' sm='1/2'>
            <div style={{padding: '15px 0', backgroundColor: Color.material.orange600, color: Color.material.white}}>Left column, pushed to the right</div>
          </Col>
          <Col pullSm='1/2' sm='1/2'>
            <div style={{padding: '15px 0', backgroundColor: Color.material.indigo600, color: Color.material.white}}>Right column, pulled to the left</div>
          </Col>
          <Col pushMd='1/4' sm='1/2' styles={{marginTop: 24}}>
            <div style={{padding: '15px 0', backgroundColor: Color.material.lime600, color: Color.material.white}}>One half, offsetted 25%</div>
          </Col>
        </Row>
      </Container>
    );
  }
}
