import React, { Component } from 'react';
import axios from 'axios';
import Color from '../../src/color';
import { Container } from '../../src/Rubrick';

class LastData extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tracks: []
    }
  }

  componentDidMount() {
    const url = 'http://ws.audioscrobbler.com/2.0/?method=user.gettoptracks&user='+this.props.user+'&period='+this.props.period+'&api_key=9238ac738b0bb19ac8473e129a866444&format=json';

    axios.get(url)
      .then((response) => {
        let tracks = response.data.toptracks.track.map((item) => {
          return {
            id: item.mbid,
            name: item.name,
            artist: item.artist.name,
            playcount: item.playcount,
            image: item.image[0]['#text'],
            rank: item['@attr'].rank
          }
        })
        this.setState({ tracks })
      })
  }

  render() {
    let tracks = this.state.tracks.map((item, index) => {
      return (
        <tr key={index} >
          <td>{item.rank}</td>
          <td><img src={item.image} alt={item.name} style={{ display: 'block' }} /></td>
          <td>{item.artist}</td>
          <td>{item.name}</td>
          <td>{item.playcount}</td>
        </tr>
      )
    })

    return (
      <table>
        <colgroup>
          <col width='60' />
          <col width='60' />
          <col width='' />
          <col width='' />
          <col width='80' />
        </colgroup>
        <thead>
          <tr>
            <th>Rank</th>
            <th></th>
            <th>Artist</th>
            <th>Track</th>
            <th>Played</th>
          </tr>
        </thead>
        <tbody>
          { tracks }
        </tbody>
      </table>
    )
  }
}

export class Test extends Component {

  render() {
    return (
      <Container>
        <h2>Table</h2>
        <LastData user='intzbeautykillz' period='1month' />
      </Container>
    )
  }
}
