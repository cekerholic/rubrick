import React, { Component } from 'react';
import axios from 'axios';
import Color from '../../src/color';
import { Container, Hexagon } from '../../src/Rubrick';

class PictureList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pictures: []
    }
  }

  componentDidMount() {
    let url = 'https://api.500px.com/v1/photos/search?term='+this.props.term+'&page=1&rpp=20&image_size=440&sort=newest&consumer_key=sPvXEpW2sFrch65rpyZQf01lBHuRGkEDDROTG1r4';

    axios.get(url)
      .then((response) => {
        let pictures = response.data.photos.map((item) => {
          return {
            id: item.id,
            name: item.name,
            src: item.image_url,
            url: item.url
          }
        })
        this.setState({ pictures })
      })

  }

  render() {
    let pictures = this.state.pictures.map((item, index) => {
      return (
        <a key={index} target='_blank' href={"https://500px.com" + item.url} title={item.name} >
          <Hexagon borderWidth={1} styles={{backgroundImage: 'url('+item.src+')', marginBottom: 100}} />
        </a>
      )
    })

    return (
      <div>
        { pictures }
      </div>
    )
  }
}

export class Home extends Component {

  render() {
    return (
      <Container>
        <h1>Welcome to Home</h1>
        <PictureList term='landscape' />
      </Container>
    )
  }
}
