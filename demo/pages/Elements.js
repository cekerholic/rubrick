import React, { Component } from 'react';
import Color from '../../src/color';
import {
  Container,
  Row,
  Col,
  CircularImage,
  Label,
  Card,
  EnhancedImage
} from '../../src/Rubrick';

export class Elements extends Component {

  render() {
    return (
      <Container>
        <h2>Circular Image</h2>
        <CircularImage src="https://s3.amazonaws.com/uifaces/faces/twitter/zeldman/128.jpg"       size={128} style={{marginRight: 12}} />
        <CircularImage src="https://s3.amazonaws.com/uifaces/faces/twitter/sauro/128.jpg"         size={96}  style={{marginRight: 12}} />
        <CircularImage src="https://s3.amazonaws.com/uifaces/faces/twitter/sindresorhus/128.jpg"  size={64}  style={{marginRight: 12}} />

        <h2>Label</h2>
        <Label styles={{marginRight: 10}}>                                                This is a label </Label>
        <Label styles={{marginRight: 10}} backgroundColor={Color.material.deepOrange500}> This is a label </Label>
        <Label styles={{marginRight: 10}} backgroundColor={Color.material.indigo500}>     This is a label </Label>
        <Label styles={{marginRight: 10}} backgroundColor={Color.material.lightGreen500}> This is a label </Label>
        <Label styles={{marginRight: 10}} backgroundColor={Color.material.pink500}>       This is a label </Label>
        <Label styles={{marginRight: 10}} backgroundColor={Color.material.amber500}>      This is a label </Label>
        <Label styles={{marginRight: 10}} backgroundColor={Color.material.grey500}>       This is a label </Label>

        <h2>Card</h2>
        <Row>
          <Col md="1/3" styles={{marginBottom: 24}}>
            <Card shadow={0}>
              <EnhancedImage image="https://unsplash.it/900/450/?random" height={500} width={1000} />
              <div style={{padding: 12}}>
                <h3>Shadow Depth 0</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium exercitationem, fugiat culpa, itaque eum illo omnis quis debitis dolorum distinctio. Repudiandae molestias nobis ipsa neque porro veritatis eaque est, aut.</p>
              </div>
            </Card>
          </Col>
          <Col md="1/3" styles={{marginBottom: 24}}>
            <Card>
              <EnhancedImage image="https://unsplash.it/1000/500/?random" height={500} width={1000} />
              <div style={{padding: 12}}>
                <h3>Shadow Depth 1</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium exercitationem, fugiat culpa, itaque eum illo omnis quis debitis dolorum distinctio. Repudiandae molestias nobis ipsa neque porro veritatis eaque est, aut.</p>
              </div>
            </Card>
          </Col>
          <Col md="1/3" styles={{marginBottom: 24}}>
            <Card shadow={2}>
              <EnhancedImage image="https://unsplash.it/1100/550/?random" height={500} width={1000} />
              <div style={{padding: 12}}>
                <h3>Shadow Depth 2</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium exercitationem, fugiat culpa, itaque eum illo omnis quis debitis dolorum distinctio. Repudiandae molestias nobis ipsa neque porro veritatis eaque est, aut.</p>
              </div>
            </Card>
          </Col>
          <Col md="1/3" styles={{marginBottom: 24}}>
            <Card shadow={3}>
              <EnhancedImage image="https://unsplash.it/1200/600/?random" height={500} width={1000} />
              <div style={{padding: 12}}>
                <h3>Shadow Depth 3</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium exercitationem, fugiat culpa, itaque eum illo omnis quis debitis dolorum distinctio. Repudiandae molestias nobis ipsa neque porro veritatis eaque est, aut.</p>
              </div>
            </Card>
          </Col>
          <Col md="1/3" styles={{marginBottom: 24}}>
            <Card shadow={4}>
              <EnhancedImage image="https://unsplash.it/1300/650/?random" height={500} width={1000} />
              <div style={{padding: 12}}>
                <h3>Shadow Depth 4</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium exercitationem, fugiat culpa, itaque eum illo omnis quis debitis dolorum distinctio. Repudiandae molestias nobis ipsa neque porro veritatis eaque est, aut.</p>
              </div>
            </Card>
          </Col>
          <Col md="1/3" styles={{marginBottom: 24}}>
            <Card shadow={5}>
              <EnhancedImage image="https://unsplash.it/broken" height={2} width={3} />
              <div style={{padding: 12}}>
                <h3>Shadow Depth 5</h3>
                <p>Broken image, notice the area of the image is still preserved.</p>
              </div>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}
