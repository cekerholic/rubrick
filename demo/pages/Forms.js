import React, { Component, PropTypes } from 'react';
import Color from '../../src/color';
import { Container, Row, Col, MaterialForm } from '../../src/Rubrick';

export class Forms extends Component {
  render() {
    return (
      <Container>
        <Row styles={{marginTop: 20, marginBottom: 20}}>
          <Col>
            <h2>Material TextField</h2>
          </Col>
          <Col sm='1/3'>
            <MaterialForm label='Input Text' id='field0' />
            <br />
            <MaterialForm label='Input Text with hint' id='field3' hint='This is the hint' />
            <br />
            <MaterialForm label='Input Text with custom color' focusColor={Color.material.purple500}  hintColor={Color.material.purple500} id='field3' hint='This is the hint' />
          </Col>
          <Col sm='1/3'>
            <MaterialForm label='Input Text with maxLength (20)' id='field1' maxLength={20} />
            <br />
            <MaterialForm id='field4' disabled value='Disabled with value' />
          </Col>
          <Col sm='1/3'>
            <MaterialForm label='Textarea' id='field2' multiline />
          </Col>
        </Row>
      </Container>
    );
  }
}
