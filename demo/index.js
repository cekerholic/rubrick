import '../sass/main.scss';

import React, { Component } from 'react';
import { browserHistory, IndexRoute, Router, Route } from 'react-router';
import { render } from 'react-dom';

import { Home } from './pages/Home';
import { Grid } from './pages/Grid';
import { Forms } from './pages/Forms';
import { Test } from './pages/Test';
import { Buttons } from './pages/Buttons';
import { Elements } from './pages/Elements';

import { NavLink } from './pages/modules/NavLink';

import { NavBar } from '../src/components/NavBar';
import { NavItem } from '../src/components/NavItem';

class App extends Component {
  render() {
    return (
      <div>
        <NavBar contained>
          <NavItem><NavLink to='/' onlyActiveOnIndex>Home</NavLink></NavItem>
          <NavItem><NavLink to='/grid'>Grid</NavLink></NavItem>
          <NavItem><NavLink to='/forms'>Forms</NavLink></NavItem>
          <NavItem><NavLink to='/test'>Test</NavLink></NavItem>
          <NavItem><NavLink to='/buttons'>Buttons</NavLink></NavItem>
          <NavItem><NavLink to='/elements'>UI Elements</NavLink></NavItem>
        </NavBar>

        {this.props.children}
      </div>
    );
  }
}

render((
  <Router history={browserHistory}>
    <Route path='/' component={App}>

      <IndexRoute component={Home} />

      <Route path='/grid' component={Grid} />
      <Route path='/forms' component={Forms} />
      <Route path='/test' component={Test} />
      <Route path='/buttons' component={Buttons} />
      <Route path='/elements' component={Elements} />
    </Route>
  </Router>
  ), document.getElementById('root')
);
