// Responsive Breakpoints
exports.breakpoint = {
  xs: 320,
  sm: 480,
  md: 768,
  lg: 980,
  xl: 1230
}

// Grid Container & Gutter
exports.width = {
  container: 1152,
  gutter: 24
}

// Grid Columns
function percent(n) {
  return (n * 100) + '%';
}

function denominators(n) {
  for (let d = 2; d <= 20; d++) {
    if (n < d) {
      exports.fractions[n + '/' + d] = percent(n / d);
    }
  }
}

exports.fractions = {};

for (let numerator = 1; numerator <= 19; numerator++) {
  denominators(numerator);
}
