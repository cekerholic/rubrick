import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import Color from '../../src/color';

export class Hexagon extends Component {
  static propTypes = {
    backgroundColor: PropTypes.string,
    borderColor: PropTypes.string,
    borderWidth: PropTypes.number,
    className: PropTypes.string,
    children: PropTypes.node,
    size: PropTypes.number,
    styles: PropTypes.object,
  };

  static defaultProps = {
    backgroundColor: 'transparent',
    borderWidth: 0,
    borderColor: Color.material.teal500,
    size: 200,
  };

  render() {
    let { backgroundColor, borderColor, borderWidth, className, children, size, styles } = this.props;

    let hexagonClass = classNames(
      'hexa',
      (className ? className : null)
    );

    let computedHeight = Math.floor((size / 2) /.86);

    let sharedStyle = {
      backgroundColor,
      height: computedHeight,
      width: size,
      borderLeftWidth: borderWidth,
      borderRightWidth: borderWidth,
      borderTopWidth: 0,
      borderBottomWidth: 0,
      borderColor,
      borderStyle: 'solid',
      marginTop: computedHeight / 2,
      backgroundSize: 'auto ' + (computedHeight * 2) + 'px'
    };

    let pseudoStyle = {
      width: size,
      borderLeftWidth: borderWidth,
      borderRightWidth: borderWidth,
      borderTopWidth: 0,
      borderBottomWidth: 0,
      borderColor,
      borderStyle: 'solid',
      left: borderWidth * -1,
    };

    return (
      <div className={hexagonClass} style={Object.assign(sharedStyle, styles)} >
        <div className='hexa__before' style={pseudoStyle} />
          {children}
        <div className='hexa__after' style={pseudoStyle} />
      </div>
    );
  }
}
