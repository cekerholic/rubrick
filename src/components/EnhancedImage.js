import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import Val from '../constants';

export class EnhancedImage extends Component {
  static propTypes = {
    alt: PropTypes.string,
    className: PropTypes.string,
    image: PropTypes.string,
    height: PropTypes.number,
    width: PropTypes.number,
    styles: PropTypes.object,
  };

  static defaultProps = {
    alt: ''
  }

  render() {
    let { alt, className, image, height, width, styles, ...rest } = this.props;

    let enhancedImageClass = classNames(
      'enhanced-image',
      (classNames ? classNames : null)
    );

    let imageHeight = {
      paddingBottom: (height/width) * 100 + '%',
    }

    return (
      <div
        className={enhancedImageClass}
        style={Object.assign(imageHeight, styles)} >
        <img alt={alt} className="enhanced-image__img" src={image} />
      </div>
    );
  }
}
