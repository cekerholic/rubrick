import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import Textarea from 'react-textarea-autosize';
import Color from '../color';

export class MaterialForm extends Component {
  static propTypes = {
    color: PropTypes.string,
    disabled: PropTypes.bool,
    focusColor: PropTypes.string,
    hint: PropTypes.string,
    hintColor: PropTypes.string,
    hintError: PropTypes.bool,
    id: PropTypes.string,
    label: PropTypes.string,
    maxLength: PropTypes.number,
    multiline: PropTypes.bool,
    styles: PropTypes.object,
    type: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string
    ]),
  };

  static defaultProps = {
    type: 'text',
    color: Color.material.grey400,
    focusColor: Color.material.teal500,
    hintColor: Color.material.teal500,
    value: '',
  };

  constructor(props) {
    super(props);

    this.state = {
      styles: this.props.styles,
      value: this.props.value
    }
  }

  focusHandler = (e) => {
    this.setState({
      styles: {
        borderBottomColor: this.props.focusColor,
        boxShadow: '0 1px 0 ' + this.props.focusColor,
        maxHeight: 'none',
      },
      labelStyle: {
        color: this.props.focusColor,
        fontSize: '1.2rem',
        transform: 'translateY(-2rem)',
      }
    })
  }

  changeHandler = (e) => {
    this.setState({
      value: e.target.value
    })
  }

  blurHandler = (e) => {
    if (this.state.value === '') {

      this.setState({
        styles: {
          borderBottomColor: this.props.color
        },
        labelStyle: {
          color: this.props.color,
          fontSize: '1.6rem',
          transform: 'translateY(0)'
        }
      })
    }
  }

  render() {
    let { color, disabled, focusColor, hint,
      hintColor, hintError, id, label, maxLength,
      multiline, styles, type, value, ...rest } = this.props;

    let fieldStyle = {
      borderBottomColor: color,
      minHeight: 49
    }

    if (disabled) {
      fieldStyle = {
        borderBottomStyle: 'dashed',
        color,
      }
    }

    let labelStyle = {
      color,
    }

    let inputType = 'text';

    let inputLabel = label ? (
      <label
        className="material-form__label"
        htmlFor={id}
        style={Object.assign(labelStyle, this.state.labelStyle)}
      >
        {label}
      </label>
    ) : null;

    let hintClass = classNames(
      'material-form__hint',
      (hintError ? 'is-error' : 'is-success')
    );

    let hintStyle = {
      color: hintColor,
    }

    if(hintError) {
      hintStyle = {
        color: Color.material.red500,
      }
    }

    let hintText = hint ? (
      <div
        className={hintClass}
        style={hintStyle}>
        {hint}
      </div>
    ) : null;


    let TextField = 'input';

    if (multiline) {
      TextField = Textarea;
    }

    return (
      <div className='material-form'>
        {inputLabel}
        <TextField
          className='material-form__field'
          style={Object.assign(fieldStyle, this.state.styles)}
          onFocus={this.focusHandler}
          onChange={this.changeHandler}
          onBlur={this.blurHandler}
          disabled={disabled}
          maxLength={maxLength}
          {...rest}
          label={null} />
        {hintText}
      </div>
    );
  }
}
