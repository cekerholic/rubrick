import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

export class NavItem extends Component {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node.isRequired,
  }

  render() {
    let { className, children } = this.props;

    let navitemClass = classNames(
      'nav__item',
      (className ? className : null)
    );

    return <li className={navitemClass} {...this.props} />;
  }
}
