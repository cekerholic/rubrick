import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import Val from '../constants';

export class Container extends Component {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node.isRequired,
    gutter: PropTypes.number,
    maxWidth: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    styles: PropTypes.object
  };

  static defaultProps = {
    maxWidth: Val.width.container,
    gutter:   Val.width.gutter,
  };

  render() {
    let { className, children, gutter, maxWidth, styles, ...rest } = this.props;

    let containerStyle = {
      maxWidth,
      paddingLeft: (gutter/2),
      paddingRight: (gutter/2),
    }

    let containerClass = classNames(
      'container',
      (className ? className : null)
    );

    return (
      <div
        className={containerClass}
        style={Object.assign(containerStyle, styles)}
        {...rest}>
          {children}
      </div>
    );
  }
}
