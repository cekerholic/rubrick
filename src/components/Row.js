import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import Val from '../constants';

export class Row extends Component {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node.isRequired,
    gutter: PropTypes.number,
    styles: PropTypes.object
  };

  static defaultProps = {
    gutter: Val.width.gutter,
  }

  render() {
    let { className, children, gutter, styles, ...rest } = this.props;
    let rowClass = classNames(
      'row',
      (className ? className : null)
    );

    let rowStyle = {
      marginLeft:  (gutter/-2),
      marginRight: (gutter/-2),
    }

    return (
      <div
        className={rowClass}
        style={Object.assign(rowStyle, styles)}
        {...rest} >
        {children}
      </div>
    );
  }
}
