import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import Color from '../color';

export class Card extends Component {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node.isRequired,
    padding: PropTypes.number,
    radii: PropTypes.number,
    shadow: PropTypes.oneOf([0, 1, 2, 3, 4, 5]),
    styles: PropTypes.object,
  };

  static defaultProps = {
    padding: 0,
    radii: 2,
    shadow: 1
  };

  render() {
    let { className, children, padding, radii, shadow, styles, ...rest } = this.props;

    let cardClass = classNames(
      'card',
      (shadow ? 'z-depth-' + shadow : null),
      (classNames ? classNames : null)
    );

    let paddingSize = { padding };

    let radiiSize = { borderRadius: radii };

    return (
      <div
        className={cardClass}
        style={Object.assign(
          paddingSize,
          radiiSize,
          styles)}
        >
        {children}
      </div>
    );
  }
}
