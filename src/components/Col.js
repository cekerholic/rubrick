import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import Val from '../constants';

export class Col extends Component {
  static propTypes = {
    className: PropTypes.string,
    gutter: PropTypes.number,
    styles: PropTypes.object,

    xs: PropTypes.string,
    sm: PropTypes.string,
    md: PropTypes.string,
    lg: PropTypes.string,

    pushXs: PropTypes.string,
    pushSm: PropTypes.string,
    pushMd: PropTypes.string,
    pushLg: PropTypes.string,

    pullXs: PropTypes.string,
    pullSm: PropTypes.string,
    pullMd: PropTypes.string,
    pullLg: PropTypes.string,
  };

  static defaultProps = {
    gutter: Val.width.gutter,
  }

  constructor(props) {
    super(props);

    this.state = {
      windowWidth: window.innerWidth
    }
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResize)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize)
  }

  handleResize = (e) => {
    this.setState({
      windowWidth: window.innerWidth
    })
  }

  render() {
    let { className, gutter, styles,
      xs, sm, md, lg,
      pushXs, pushSm, pushMd, pushLg,
      pullXs, pullSm, pullMd, pullLg,
      ...rest } = this.props;

    let { windowWidth } = this.state;

    let columnStyle = {
      paddingLeft:  (gutter/2),
      paddingRight: (gutter/2),
    }

    let columnClass = classNames(
      'row__column',
      (className ? className : null)
    )

    if (windowWidth < Val.breakpoint.sm) {
      columnStyle.width = xs;
      columnStyle.left = pushXs;
      columnStyle.right = pullXs;
    } else if (windowWidth < Val.breakpoint.md) {
      columnStyle.width = sm || xs;
      columnStyle.left = pushSm || pushXs;
      columnStyle.right = pullSm || pullXs;
    } else if (windowWidth < Val.breakpoint.lg) {
      columnStyle.width = md || sm || xs;
      columnStyle.left = pushMd || pushSm || pushXs;
      columnStyle.right = pullMd || pullSm || pullXs;
    } else {
      columnStyle.width = lg || md || sm || xs;
      columnStyle.left = pushLg || pushMd || pushSm || pushXs;
      columnStyle.right = pullLg || pullMd || pullSm || pullXs;
    }

    if (columnStyle.width in Val.fractions) {
      columnStyle.width = Val.fractions[columnStyle.width];
    }

    if (!columnStyle.width) {
      columnStyle.width = '100%';
    }

    if (columnStyle.right in Val.fractions) {
      columnStyle.right = Val.fractions[columnStyle.right];
    }
    if (columnStyle.left in Val.fractions) {
      columnStyle.left = Val.fractions[columnStyle.left];
    }

    return <div
      className={columnClass}
      style={Object.assign(columnStyle, styles)}
      {...rest} />;

  }
}
