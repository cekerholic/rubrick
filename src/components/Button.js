import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import Ink from 'react-ink';
import Val from '../constants';
import Color from '../color';

export class Button extends Component {
  static propTypes = {
    anchor: PropTypes.bool,
    backgroundColor: PropTypes.string,
    block: PropTypes.bool,
    children: PropTypes.node,
    className: PropTypes.string,
    disabled: PropTypes.bool,
    radii: PropTypes.number,
    shade: PropTypes.oneOf(['light', 'dark']),
    size: PropTypes.oneOf(['xs', 'sm', 'md', 'lg']),
    styles: PropTypes.object,
    type: PropTypes.string,
    value: PropTypes.string,
  };

  static defaultProps = {
    size: 'md',
    backgroundColor: Color.material.white,
    radii: 2,
    shade: 'light',
  };

  render() {
    let { backgroundColor, children, className, anchor, block, disabled, radii, shade, size, styles, type, value, ...rest } = this.props;

    let buttonStyle = {
      color: textColor,
      backgroundColor,
      borderRadius: radii,
    }

    let textColor = {
      color: Color.material.grey700
    }

    if (shade === 'dark') {
      textColor.color = Color.material.white
    }

    let buttonSizeStyle = {
      padding: '.8rem 1.6rem',
      fontSize: '1.4rem',
      lineHeight: (2/1.4),
    }

    if (size === 'xs') {
      buttonSizeStyle = {
        padding: '.6rem 1.2rem',
        fontSize: '1.08rem',
        lineHeight: (1.5/1.08),
      }
    } else if (size === 'sm') {
      buttonSizeStyle = {
        padding: '.6rem 1.2rem',
        fontSize: '1.3rem',
        lineHeight: (2/1.3),
      }
    } else if (size === 'lg') {
      buttonSizeStyle = {
        padding: '1.2rem 2.4rem',
        fontSize: '1.8rem',
        lineHeight: (2.4/1.8),
      }
    }

    let buttonClass = classNames(
      'button',
      (block ? 'button--block' : null),
      (shade ? 'button--' + shade : null),
      (className ? className : null),
    );

    let Element = 'button';
    if (anchor) {
      Element = 'a'
    }

    let disabledStyle = {
      boxShadow: 'none',
      opacity: .5,
    }

    return (
      <Element
        className={buttonClass}
        style={Object.assign(
          buttonStyle,
          textColor,
          buttonSizeStyle,
          (disabled ? disabledStyle : null),
          styles
        )}
        {...rest} >
          <span className='button__layer' style={{borderRadius: radii}} />
          <span className='button__text'>{children}</span>
          {(disabled ? null : <Ink style={{zIndex: 2}} /> )}
      </Element>
    );
  }
}
