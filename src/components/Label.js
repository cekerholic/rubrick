import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import Color from '../color';

export class Label extends Component {
  static propTypes = {
    backgroundColor: PropTypes.string,
    color: PropTypes.string,
    className: PropTypes.string,
    styles: PropTypes.object,
  };

  static defaultProps = {
    backgroundColor: Color.material.teal500,
    color: Color.material.white
  };

  render() {
    let { backgroundColor, color, className, styles, ...rest } = this.props;

    let labelClass = classNames(
      'label',
      (classNames ? classNames : null)
    );

    let labelStyle = {
      backgroundColor,
      color
    };

    return <div
      className={labelClass}
      style={Object.assign(labelStyle, styles)}
      {...rest} />;
  }
}
