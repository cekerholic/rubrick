import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import Val from '../constants';

export class CircularImage extends Component {
  static propTypes = {
    alt: PropTypes.string,
    className: PropTypes.string,
    image: PropTypes.string,
    size: PropTypes.number,
  };

  static defaultProps = {
    alt: '',
    size: 60
  }

  render() {
    let { alt, className, image, size, ...rest } = this.props;

    let circularImageClass = classNames(
      'circle',
      (classNames ? classNames : null)
    );

    return <img
      alt={alt}
      className={circularImageClass}
      src={image}
      width={size}
      {...rest} />;
  }
}
