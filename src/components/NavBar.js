import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import { Container } from './Container';

export class NavBar extends Component {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node.isRequired,
    contained: PropTypes.bool,
    fixed: PropTypes.bool,
    height: PropTypes.number,
    styles: PropTypes.object,
    title: PropTypes.node,
  };

  static defaultProps = {
    fixed: false,
  };

  render() {
    let { className, children, contained, fixed, styles, title, ...rest } = this.props;

    let navbarClass = classNames(
      'navbar',
      (className ? className : null)
    );

    let navStyle = {}

    if (fixed) {
      navStyle = {
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        zIndex: 999
      }
    }

    let navChild = (
      <div>
        {(title ? title : null)}
        <ul className='nav'>
          {children}
        </ul>
      </div>
    )

    return (
      <nav
        className={navbarClass}
        style={Object.assign(navStyle, styles)}
        {...rest}>
          {(contained ? <Container>{navChild}</Container> : navChild )}
      </nav>
    );
  }
}
